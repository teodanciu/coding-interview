name := "coding-interview"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
    "joda-time" % "joda-time" % "2.9.2",
    "org.joda" % "joda-convert" % "1.8",
    "com.typesafe.scala-logging" % "scala-logging_2.11" % "3.1.0",
    "ch.qos.logback" % "logback-classic" % "1.1.2",
    "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)
