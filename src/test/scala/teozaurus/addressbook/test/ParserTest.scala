package teozaurus.addressbook
package test

import org.joda.time.LocalDate
import org.scalatest.{FunSuite, Inside, Matchers, TryValues}
import teozaurus.date.Dates
import Dates.format
import Gender.{Female, Male}
import parser._
import Parser._

class ParserTest extends FunSuite with Matchers with Inside with TryValues {

  test("Split line") {
    splitLine("Bill McKnight  , Male, 16/03/77") shouldBe
      Seq("Bill McKnight", "Male", "16/03/77")
  }

  test("Split name") {
    parseNames("Bill") shouldBe Seq("Bill")
  }
  test("Split two names") {
    parseNames("McKnight Bill") shouldBe Seq("McKnight", "Bill")
  }
  test("Split three names") {
    parseNames("McKnight Bill A") shouldBe Seq("McKnight", "Bill", "A")
  }

  test("Sample input") {
    val lines = Iterator(
      "Bill McKnight, Male, 16/03/77",
      "Paul Robinson, Male, 15/01/85",
      "Gemma Lane, Female, 20/11/91",
      "Sarah Stone, Female, 20/09/80",
      "Wes Jackson, Male, 14/08/74"
    )

    val result = Parser.parse(lines)
    result.addressBook shouldBe AddressBookFixture.sampleAddressBook
    result.parseFailures shouldBe empty
  }

  test("Single name") {
    val lines = Iterator("Bill, Male, 16/03/77")
    Parser.parse(lines).addressBook shouldBe AddressBook(Seq(Contact(
      "Bill",
      Male,
      LocalDate.parse("16/03/77", format)
    )))
  }

  test("Invalid gender") {
    val lines = Iterator("Bill McKnight, Mail, 16/03/77")
    val result = Parser.parse(lines)
    result.addressBook.isEmpty shouldBe true
    inside(result.parseFailures.headOption) {
      case Some(InvalidInput(i, e)) =>
        i shouldBe "Bill McKnight, Mail, 16/03/77"
        e shouldBe a [InvalidGenderException]
    }
  }

  test("Partial addressbook") {
    val lines = Iterator(
      "Bill McKnight, Male, 16/03/77",
      "Paul Robinson, Mail, 15/01/85",
      "Gemma Lane, Female, 20/11/91",
      "Sarah Stone Female, 20/09/80",
      "Wes Jackson, Male, 14/0/74"
    )
    val result = Parser.parse(lines)
    result.addressBook shouldBe AddressBook(Seq(
      Contact("Bill McKnight", Male, LocalDate.parse("16/03/77", format)),
      Contact("Gemma Lane", Female, LocalDate.parse("20/11/91", format))
    ))
    val failures = result.parseFailures
    failures.size shouldBe 3
    failures.head.input shouldBe "Paul Robinson, Mail, 15/01/85"
    failures.head.t shouldBe a [InvalidGenderException]

    failures.tail.head.input shouldBe "Sarah Stone Female, 20/09/80"
    failures.tail.head.t shouldBe a [InvalidFormatException]

    failures.last.input shouldBe "Wes Jackson, Male, 14/0/74"
    failures.last.t shouldBe a [InvalidDateException]
  }

}
