package teozaurus.addressbook
package test

import org.joda.time.LocalDate
import teozaurus.date.Dates
import Dates._
import Gender._

object AddressBookFixture {

  val sampleAddressBook = AddressBook(Seq(
    Contact("Bill McKnight", Male, LocalDate.parse("16/03/77", format)),
    Contact("Paul Robinson", Male, LocalDate.parse("15/01/85", format)),
    Contact("Gemma Lane", Female, LocalDate.parse("20/11/91", format)),
    Contact("Sarah Stone", Female, LocalDate.parse("20/09/80", format)),
    Contact("Wes Jackson", Male, LocalDate.parse("14/08/74", format))
  ))
}
