package teozaurus.addressbook
package test

import AddressBookFixture.sampleAddressBook
import Queries._
import org.scalatest.{OptionValues, Matchers, FunSuite}

class QueriesTest extends FunSuite with Matchers with OptionValues {

  test("number of males") {
    numberOfMales(sampleAddressBook) shouldBe 3
  }

  test("oldest") {
    oldest(sampleAddressBook).map(_.name).value shouldBe "Wes Jackson"
  }

  test("age difference - full names") {
    ageDifferenceInDays("Bill McKnight", "Paul Robinson")(sampleAddressBook).value shouldBe 2862
  }

  test("age difference - names as required") {
    ageDifferenceInDays("Bill", "Paul")(sampleAddressBook).value shouldBe 2862
  }
}
