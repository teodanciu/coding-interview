package teozaurus.addressbook.test

import org.scalatest.{FunSuite, Matchers}
import AddressBookFixture._

class AddressBookTest extends FunSuite with Matchers {

  test("address book lookup") {
    sampleAddressBook.lookup(Seq("Bill")) shouldBe sampleAddressBook.contacts.headOption
    sampleAddressBook.lookup(Seq("McKnight")) shouldBe sampleAddressBook.contacts.headOption
    sampleAddressBook.lookup("Bill McKnight") shouldBe sampleAddressBook.contacts.headOption
    sampleAddressBook.lookup(Seq("Bill", "McKnight")) shouldBe sampleAddressBook.contacts.headOption
  }

}
