package teozaurus.journalranking
package test

import org.scalatest.{Matchers, FunSuite}

class RankerTest extends FunSuite with Matchers {
  test("Scenario 1") {
    val journals = Seq(
      Journal("Journal A", 5.6),
      Journal("Journal B", 2.4),
      Journal("Journal C", 3.1)
    )
    val result = Ranker.rankMaintainingAbsoluteIndex(journals)
    result shouldBe RankingResult(Seq(
      RankEntry(1, Journal("Journal A", 5.6)),
      RankEntry(2, Journal("Journal C", 3.1)),
      RankEntry(3, Journal("Journal B", 2.4))
    ))
  }

  test("Scenario 2") {
    val journals = Seq(
      Journal("Journal A", 2.2),
      Journal("Journal C", 6.2),
      Journal("Journal B", 6.2)
    )
    val result = Ranker.rankMaintainingAbsoluteIndex(journals)
    result shouldBe RankingResult(Seq(
      RankEntry(1, Journal("Journal B", 6.2)),
      RankEntry(1, Journal("Journal C", 6.2)),
      RankEntry(3, Journal("Journal A", 2.2))
    ))
  }

  test("Scenario 3") {
    val journals = Seq(
      Journal("Journal A", 5.6, isReview = true),
      Journal("Journal B", 2.4),
      Journal("Journal C", 3.1)
    )
    val result = Ranker.rankMaintainingAbsoluteIndex(journals)
    result shouldBe RankingResult(Seq(
      RankEntry(1, Journal("Journal C", 3.1)),
      RankEntry(2, Journal("Journal B", 2.4))
    ))
  }

  test("Journals with the same score") {
    val journals = Seq(
      Journal("Journal A", 5.6),
      Journal("Journal B", 5.6),
      Journal("Journal C", 5.6)
    )
    val result = Ranker.rankMaintainingAbsoluteIndex(journals)
    result shouldBe RankingResult(Seq(
      RankEntry(1, Journal("Journal A", 5.6)),
      RankEntry(1, Journal("Journal B", 5.6)),
      RankEntry(1, Journal("Journal C", 5.6))
    ))
  }
}
