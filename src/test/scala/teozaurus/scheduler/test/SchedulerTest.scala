package teozaurus
package scheduler
package test

import org.scalatest.{TryValues, Matchers, FunSuite}
import SchedulerFixture._
import teozaurus.scheduler.model.Calendar
import date.Dates._

class SchedulerTest extends FunSuite with Matchers with TryValues {

  test("Sample booking requests") {
    val calendar = Scheduler.schedule(sampleBatch)
    checkDayEntriesAreSorted(calendar)
    checkBookingsDoNotOverlap(calendar)
    calendar shouldBe sampleCalender
  }

  test("Sample 2") {
    val lines =
     """|0900 1730
        |2011-03-17 07:17:06 EMP009
        |2011-03-21 08:00 2
        |2011-03-17 10:17:06 EMP001
        |2011-03-21 09:00 2
        |2011-03-18 10:17:06 EMP006
        |2011-03-21 09:00 2
        |2011-03-16 12:34:56 EMP002
        |2011-03-21 11:00 2
        |2011-03-16 09:28:23 EMP003
        |2011-03-22 14:00 2
        |2011-03-17 10:17:06 EMP004
        |2011-03-22 16:00 1
        |2011-03-15 17:29:12 EMP005
        |2011-03-21 16:00 3""".stripMargin.split("\n").iterator
    val calendar = Scheduler.schedule(Parser.parse(lines).get)
    checkDayEntriesAreSorted(calendar)
    checkBookingsDoNotOverlap(calendar)
    calendar.unsuccessfulRequests.map(_.submission.employeeId).toSet shouldBe Set("EMP005", "EMP009", "EMP006")
  }

  def checkDayEntriesAreSorted(calendar: Calendar) = {
    val entries = calendar.entries
    entries.sortBy(_.day) shouldBe entries
    entries.foreach { entry =>
      entry.bookings.sortBy(_.interval.getStart) shouldBe entry.bookings
    }
  }

  def checkBookingsDoNotOverlap(calendar: Calendar) = {
    calendar.entries.foreach { entry =>
      for (i <- 0 until entry.bookings.size - 1) {
        for (j <- i + 1 until entry.bookings.size) {
          entry.bookings(i).interval.overlaps(entry.bookings(j).interval) shouldBe false
        }
      }
    }
  }

  def checkBookingsWithinOfficeHours(calendar: Calendar) = {
    calendar.entries.flatMap(_.bookings).foreach { booking =>
      val start = booking.interval.getStart
      val end = booking.interval.getEnd
      (!start.isBefore(start.withTime(calendar.officeHours.startTime)) &&
        !end.isAfter(end.withTime(calendar.officeHours.endTime))) shouldBe true
    }
  }
}
