package teozaurus
package scheduler
package test

import date.Dates._
import Scheduler._
import model.{OfficeHours, Request}
import org.joda.time.{DateTime, Duration, LocalTime}
import org.scalatest.{FunSuite, Matchers}

class SchedulerConstraintsTest extends FunSuite with Matchers {
  test("Constraints: within office hours") {
    val officeHours = OfficeHours(
      LocalTime.parse("0900", militaryTimeFormat),
      LocalTime.parse("1730", militaryTimeFormat)
    )
    withinOfficeHours(request("08:00", 2), officeHours) shouldBe false
    withinOfficeHours(request("09:00", 2), officeHours) shouldBe true
    withinOfficeHours(request("15:30", 1), officeHours) shouldBe true
    withinOfficeHours(request("16:30", 1), officeHours) shouldBe true
    withinOfficeHours(request("16:31", 1), officeHours) shouldBe false
    withinOfficeHours(request("16:30", 2), officeHours) shouldBe false
    withinOfficeHours(request("17:00", 1), officeHours) shouldBe false
  }

  def request(time: String, durationInHours: Int): Request =
    Request(
      DateTime.parse(s"2011-03-21 $time", meetingStartDateFormat),
      Duration.standardHours(durationInHours)
    )
}
