package teozaurus
package scheduler
package test

import org.scalatest.{Inside, Matchers, FunSuite}

import scala.util.{Success, Failure}

class ParserTest extends FunSuite with Matchers with Inside {

  test("Parse given input") {
    val lines = Iterator(
      "0900 1730",
      "2011-03-17 10:17:06 EMP001",
      "2011-03-21 09:00 2",
      "2011-03-16 12:34:56 EMP002",
      "2011-03-21 09:00 2",
      "2011-03-16 09:28:23 EMP003",
      "2011-03-22 14:00 2",
      "2011-03-17 10:17:06 EMP004",
      "2011-03-22 16:00 1",
      "2011-03-15 17:29:12 EMP005",
      "2011-03-21 16:00 3"
    )
    Parser.parse(lines) match {
      case Failure(e) => e.printStackTrace()
      case Success(r) =>
        r shouldBe SchedulerFixture.sampleBatch
    }
  }

}
