package teozaurus
package scheduler
package test

import model._
import date.Dates._
import org.joda.time._

object SchedulerFixture {

  val officeHours = OfficeHours(
    LocalTime.parse("0900", militaryTimeFormat),
    LocalTime.parse("1730", militaryTimeFormat)
  )

  val requests = Seq(
    RequestSubmission(
      Submission(
        DateTime.parse("2011-03-17 10:17:06", submissionDateFormat),
        "EMP001"
      ),
      Request(
        DateTime.parse("2011-03-21 09:00", meetingStartDateFormat),
        Duration.standardHours(2)
      )
    ),
    RequestSubmission(
      Submission(
        DateTime.parse("2011-03-16 12:34:56", submissionDateFormat),
        "EMP002"
      ),
      Request(
        DateTime.parse("2011-03-21 09:00", meetingStartDateFormat),
        Duration.standardHours(2)
      )
    ),
    RequestSubmission(
      Submission(
        DateTime.parse("2011-03-16 09:28:23", submissionDateFormat),
        "EMP003"
      ),
      Request(
        DateTime.parse("2011-03-22 14:00", meetingStartDateFormat),
        Duration.standardHours(2)
      )
    ),
    RequestSubmission(
      Submission(
        DateTime.parse("2011-03-17 10:17:06", submissionDateFormat),
        "EMP004"
      ),
      Request(
        DateTime.parse("2011-03-22 16:00", meetingStartDateFormat),
        Duration.standardHours(1)
      )
    ),
    RequestSubmission(
      Submission(
        DateTime.parse("2011-03-15 17:29:12", submissionDateFormat),
        "EMP005"
      ),
      Request(
        DateTime.parse("2011-03-21 16:00", meetingStartDateFormat),
        Duration.standardHours(3)
      )
    )
  )
  val sampleBatch = SubmissionBatch(
    officeHours,
    requests
  )

  val sampleCalender = Calendar(
    officeHours,
    entries = Seq(
      DayEntry(
        day = LocalDate.parse("2011-03-21", dayFormat),
        bookings = Seq(
          Booking(
            new Interval(
              DateTime.parse("2011-03-21 09:00", meetingStartDateFormat),
              DateTime.parse("2011-03-21 11:00", meetingStartDateFormat)
            ),
            "EMP002"
          )
        )
      ),
      DayEntry(
        day = LocalDate.parse("2011-03-22", dayFormat),
        bookings = Seq(
          Booking(
            new Interval(
              DateTime.parse("2011-03-22 14:00", meetingStartDateFormat),
              DateTime.parse("2011-03-22 16:00", meetingStartDateFormat)
            ),
            "EMP003"
          ),
          Booking(
            new Interval(
              DateTime.parse("2011-03-22 16:00", meetingStartDateFormat),
              DateTime.parse("2011-03-22 17:00", meetingStartDateFormat)
            ),
            "EMP004"
          )
        )
      )
    ),
    unsuccessfulRequests = Seq(
      requests(4),
      requests(0)
    )
  )

}
