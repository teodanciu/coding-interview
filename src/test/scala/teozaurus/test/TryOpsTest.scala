package teozaurus
package test

import org.scalatest.{Inside, FunSuite, Matchers}
import scala.util.{Failure, Try, Success}

class TryOpsTest extends FunSuite with Matchers with Inside {
  test("sequence with successes") {
    val sequenced = Try.sequence(Iterator(
      Success("a"),
      Success("b"),
      Success("c")
    ))
    inside(sequenced) {
      case Success(s) => s.toList shouldBe List("a", "b", "c")
      case _ => fail
    }
  }

  test("sequence with a failure") {
    val sequenced = Try.sequence(Iterator(
      Success("a"),
      Failure(new IllegalArgumentException()),
      Success("c")
    ))
    inside(sequenced) {
      case Failure(e) => e shouldBe a[IllegalArgumentException]
      case _ => fail
    }
  }
}
