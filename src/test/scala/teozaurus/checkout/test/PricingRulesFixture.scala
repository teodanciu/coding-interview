package teozaurus.checkout.test

import teozaurus.checkout.{SpecialPrice, Pricing, PricingRules}

object PricingRulesFixture {

  val sampleRules = PricingRules(Seq(
    Pricing(
      sku = "A",
      unitPrice = 50,
      specialPrice = Some(SpecialPrice(3, 130))
    ),
    Pricing(
      sku = "B",
      unitPrice = 30,
      specialPrice = Some(SpecialPrice(2, 45))
    ),
    Pricing(
      sku = "C",
      unitPrice = 20,
      specialPrice = None
    ),
    Pricing(
      sku = "D",
      unitPrice = 15,
      specialPrice = None
    )
  ))

}
