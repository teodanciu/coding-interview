package teozaurus.checkout
package test

import PricingRulesFixture.sampleRules
import org.scalatest.{Matchers, FunSuite}

class ParserTest extends FunSuite with Matchers {

  test("parse given input") {
    val (rules, invalids) = Parser.parse(
      Iterator(
        "A     50        3 for 130",
        "B     30        2 for 45",
        "C     20",
        "D     15"
      ))
    rules shouldBe sampleRules
    invalids shouldBe empty
  }

  test("partially valid rules") {
    val (rules, invalids) = Parser.parse(
      Iterator(
        "A             3 for 130",
        "B     30        2 for 45",
        "C     ",
        "D     15"
      ))
    rules shouldBe PricingRules(Seq(
      Pricing("B", 30, Some(SpecialPrice(2, 45))),
      Pricing("D", 15, None)
    ))
    invalids shouldBe Seq("A             3 for 130", "C     ")
  }

}
