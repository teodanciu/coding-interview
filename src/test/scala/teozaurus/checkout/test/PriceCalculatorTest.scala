package teozaurus
package checkout
package test

import PricingRulesFixture.sampleRules
import org.scalatest.{TryValues, Matchers, FunSuite}

class PriceCalculatorTest extends FunSuite with Matchers with TryValues {

  test("calculate price from example") {
    val checkout = Checkout(Seq(
      Item("B"),
      Item("A"),
      Item("B")
    ))
    PriceCalculator.calculate(checkout, sampleRules).get shouldBe 95
  }

  test("apply offer twice") {
    val checkout = Checkout(Seq(
      Item("B"),
      Item("A"),
      Item("B"),
      Item("C"),
      Item("B"),
      Item("B")
    ))
    PriceCalculator.calculate(checkout, sampleRules).get shouldBe 160
  }

  test("apply offer only for the specified quantity") {
    val checkout = Checkout(Seq(
      Item("B"),
      Item("B"),
      Item("C"),
      Item("B")
    ))
    PriceCalculator.calculate(checkout, sampleRules).get shouldBe 95
  }

  test("fail if the price of one item was not found") {
    val checkout = Checkout(Seq(
      Item("B"),
      Item("B"),
      Item("X")
    ))
    PriceCalculator.calculate(checkout, sampleRules).failure.exception shouldBe a [PriceNotFoundException]
  }
}
