package teozaurus.date

import org.joda.time.{DateTime, LocalDateTime, LocalDate}
import org.joda.time.format.DateTimeFormat

object Dates {
  val format = DateTimeFormat.forPattern("dd/MM/yy")
  val militaryTimeFormat = DateTimeFormat.forPattern("HHmm")
  val submissionDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val meetingStartDateFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm")
  val dayFormat = DateTimeFormat.forPattern("yyyy-MM-dd")
  val timeFormat = DateTimeFormat.forPattern("HH:mm")

  implicit def dateOrdering: Ordering[LocalDate] = Ordering.fromLessThan(_ isBefore _)
  implicit def localDateTimeOrdering: Ordering[LocalDateTime] = Ordering.fromLessThan(_ isBefore _)
  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)
}
