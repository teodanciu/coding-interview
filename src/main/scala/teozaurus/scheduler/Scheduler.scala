package teozaurus
package scheduler

import date.Dates._
import model._
import org.joda.time.Interval
import scala.collection.SortedSet

object Scheduler {

  implicit def bookingOrder: Ordering[Booking] =
    Ordering.fromLessThan((b1, b2) => b1.interval.getStart.isBefore(b2.interval.getStart))

  def schedule(batch: SubmissionBatch): Calendar = {
    val submissionsByDay = batch.requests.groupBy(_.request.day).toSeq
    val dayEntries = submissionsByDay.map { case (day, requests) =>
      val sorted = requests.sortBy(_.submission.date)
      val (succeeded, failed) =
        sorted.foldLeft((SortedSet.empty[Booking], Seq.empty[RequestSubmission])) { case ((bookings, failed), submission) =>
          if (conflicts(submission.request, bookings) || !withinOfficeHours(submission.request, batch.officeHours))
            (bookings, submission +: failed)
          else
            (bookings + requestToBooking(submission), failed)
        }
      DayEntry(day, succeeded.toList) -> failed.reverse
    }
    Calendar(batch.officeHours, dayEntries.map(_._1), dayEntries.flatMap(_._2))
  }

  def withinOfficeHours(request: Request, officeHours: OfficeHours): Boolean =
    !request.startDate.isBefore(request.startDate.withTime(officeHours.startTime)) &&
     !request.endDate.isAfter(request.endDate.withTime(officeHours.endTime))

  def conflicts(request: Request, booking: Booking): Boolean =
    request.interval.overlaps(booking.interval)

  def conflicts(request: Request, bookings: SortedSet[Booking]): Boolean =
    bookings
      .dropWhile(!_.interval.contains(request.startDate))
      .exists(conflicts(request, _))

  def requestToBooking(rs: RequestSubmission) = Booking(
    new Interval(rs.request.startDate, rs.request.endDate),
    rs.submission.employeeId
  )
}
