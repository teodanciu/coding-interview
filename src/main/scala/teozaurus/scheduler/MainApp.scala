package teozaurus
package scheduler

import model.Calendar
import com.typesafe.scalalogging.{StrictLogging, LazyLogging}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, blocking}
import scala.io.Source
import scala.util.Failure

object MainApp extends App with StrictLogging {

  def run(fileName: String): Future[Calendar] =
    Future {
      blocking {
        val lines = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(fileName)).getLines
        val batch = Parser.parse(lines)
        batch.map { batch =>
          logger.info(s"Loaded batch submissions: $batch")
          Scheduler.schedule(batch)
        }.recoverWith {
          case e =>
            logger.error(s"Could not parse input from $fileName", e)
            Failure(e)
        }
      }.get
    }

  Await.result(
    run("submissionbatch").map(println(_)),
    1.minute
  )
}
