package teozaurus
package scheduler

import date.Dates._
import scheduler.model._
import org.joda.time.Duration
import scala.util.{Failure, Try}

object Parser {

  val officeHoursRegex = "(\\S{4}) (\\S{4})".r
  val submissionRegex = "(\\S{10} \\S{8}) (\\S+)".r
  val requestRegex = "(\\S{10} \\S{5}) (\\d+)".r

  def parse(lines: Iterator[String]): Try[SubmissionBatch] = {
    val officeHours = parseOfficeHours(lines.next())
    officeHours.flatMap {  officeHours =>
      val submissionRequests = Try.sequence { lines.sliding(2, 2).map {
        case submission :: request :: Nil =>
          parseSubmission(submission).flatMap { submission =>
            parseRequest(request).map { request =>
              RequestSubmission(submission, request)
            }
          }
        case input => Failure(new InvalidFormatException(input))
      }}
      submissionRequests.map { submissionRequests =>
        SubmissionBatch(officeHours, submissionRequests.toSeq)
      }
    }
  }

  def parseOfficeHours(s: String): Try[OfficeHours] = Try {
    val officeHoursRegex(start, end) = s
    OfficeHours(
      militaryTimeFormat.parseLocalTime(start),
      militaryTimeFormat.parseLocalTime(end)
    )
  } recoverWith {
    case e => new Failure(new InvalidOfficeHoursException(s, e))
  }

  def parseSubmission(s: String): Try[Submission] = Try {
    val submissionRegex(date, employee) = s
    Submission(
      submissionDateFormat.parseDateTime(date),
      employee
    )
  } recoverWith {
    case e => new Failure(new InvalidSubmissionFormatException(s, e))
  }

  def parseRequest(s: String): Try[Request] = Try {
    val requestRegex(startDate, duration) = s
    Request(
      meetingStartDateFormat.parseDateTime(startDate),
      Duration.standardHours(duration.toLong)
    )
  } recoverWith {
    case e => new Failure(new InvalidBookingRequestFormatException(s, e))
  }

}

class InvalidOfficeHoursException(input: String, cause: Throwable) extends Exception(s"Invalid input: $input", cause)
class InvalidSubmissionFormatException(input: String, cause: Throwable) extends Exception(s"Invalid input: $input", cause)
class InvalidBookingRequestFormatException(input: String, cause: Throwable) extends Exception(s"Invalid input: $input", cause)
class InvalidFormatException(input: Seq[String]) extends Exception(s"Invalid input: ${input.mkString("\n")}")