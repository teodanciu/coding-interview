package teozaurus
package scheduler.model

import date.Dates._
import org.joda.time._

case class RequestSubmission(
  submission: Submission,
  request: Request
) {
  override def toString =
    s"${meetingStartDateFormat.print(request.startDate)}" +
    s" submitted by ${submission.employeeId} " +
    s"at ${submissionDateFormat.print(submission.date)}"
}

case class Submission(
  date: DateTime,
  employeeId: String
)

case class Request(
  startDate: DateTime,
  duration: Duration
){
  lazy val day: LocalDate = startDate.toLocalDate
  lazy val startTime: LocalTime = startDate.toLocalTime
  lazy val endDate: DateTime = startDate.plus(duration)
  val interval = new Interval(startDate, endDate)
}

case class SubmissionBatch(
  officeHours: OfficeHours,
  requests: Seq[RequestSubmission]
)

case class OfficeHours(
  startTime: LocalTime,
  endTime: LocalTime
)

case class Calendar(
                     officeHours: OfficeHours,
                     entries: Seq[DayEntry],
                     unsuccessfulRequests: Seq[RequestSubmission]
) {
  override def toString =
    entries.map(_.toString).mkString("\n") +
    "Unsuccessful requests: \n" + unsuccessfulRequests.map(_.toString).mkString("\n")
}

case class DayEntry(
  day: LocalDate,
  bookings: Seq[Booking]
) {
  override def toString =
    s"""
       |${dayFormat.print(day)}
       |${bookings.map(_.toString).mkString("\n")}
     """.stripMargin
}

case class Booking(
  interval: Interval,
  employeeId: String
) {
  override def toString =
    s"${timeFormat.print(interval.getStart)} ${timeFormat.print(interval.getEnd)} $employeeId"
}