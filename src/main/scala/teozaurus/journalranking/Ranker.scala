package teozaurus.journalranking

object Ranker {
  def rank(journals: Seq[Journal]): RankingResult = {
    val entries = journals
      .filterNot(_.isReview)
      .groupBy(_.score).toSeq
      .sortBy { case (score, journals) => -score }
      .map { case (score, journals) => score -> journals.sortBy(_.name)}
      .zipWithIndex
      .flatMap { case ((_, journals), index) =>
        journals.map(j => RankEntry(index + 1, j))
      }
    RankingResult(entries)
  }

  def rankMaintainingAbsoluteIndex(journals: Seq[Journal]): RankingResult = {
    val entries = journals
      .filterNot(_.isReview)
      .groupBy(_.score).toSeq
      .sortBy { case (score, journals) => -score }
      .map { case (score, journals) => score -> journals.sortBy(_.name)}

    type RankedJournals = Seq[(Int, Seq[Journal])]
    def loop(entries: Seq[Seq[Journal]], acc: RankedJournals, count: Int): RankedJournals =
      entries match {
        case Nil => acc
        case h +: t => loop(t, (count, h) +: acc, count + h.size)
      }

    val ranked = loop(entries.map(_._2), Nil, 1).reverse
    RankingResult(
      ranked.flatMap { case (rank, journals) =>
        journals.map(RankEntry(rank, _))
      }
    )
  }

  type Ranked[A] = (Int, Seq[A])
  def rank[A](as: Seq[A])(lt: (A, A) => Boolean): Seq[Ranked[A]] = {
    def loop(as: Seq[A], acc: Seq[Ranked[A]], rank: Int): Seq[Ranked[A]] = {
      as match {
        case Nil => acc
        case h +: t =>
          acc match {
            case Nil =>
              loop(t, Seq((rank, Seq(h))), rank)
            case (r, journals) +: rt if lt(journals.head, h) =>
              loop(t, (r + 1, Seq(h)) +: acc, r + 1)
            case (r, journals) +: rt =>
              loop(t, (r, h +: journals) +: rt, r)
          }
      }
    }
    loop(as.sortWith(lt), Seq.empty[Ranked[A]], 1)
  }
}
