package teozaurus
package journalranking

case class Journal(name: String, score: Double, isReview: Boolean = false)

case class RankEntry(rank: Int, journal: Journal)
case class RankingResult(entries: Seq[RankEntry]) {
  override def toString = entries.mkString("\n")
}



