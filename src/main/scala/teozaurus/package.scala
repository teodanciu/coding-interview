import scala.util.Try

package object teozaurus {

  implicit class TryCompanionOps(val t: Try.type) extends AnyVal {
    def sequence[A](tries: TraversableOnce[Try[A]]): Try[TraversableOnce[A]] =
      tries.foldLeft(Try(Seq.empty[A])) { (acc, t) =>
        acc.flatMap(acc => t.map(t => t +: acc))
      }.map(_.reverse)
  }
}
