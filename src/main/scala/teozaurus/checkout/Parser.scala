package teozaurus
package checkout

import scala.util.{Failure, Success, Try}

object Parser {
  val lineRegex = "(\\S+)\\s+(\\d+)\\s*(.*)".r
  val specialPriceRegex = "(\\d+)\\s+for\\s+(\\d+)".r

  def parse(lines: Iterator[String]): (PricingRules, Seq[String]) = {
    val (pricings, invalids) = lines.foldLeft(Seq.empty[Pricing], Seq.empty[String]) { case ((pricing, invalids), line) =>
      parseLine(line) match {
        case Success(p) =>
          (p +: pricing, invalids)
        case Failure(e) =>
          (pricing, line +: invalids)
      }
    }
    (PricingRules(pricings.reverse), invalids.reverse)
  }

  def parseLine(s: String): Try[Pricing] = {
    s match {
      case lineRegex(sku, unitPrice, special) if special.isEmpty =>
        Success(Pricing(sku, unitPrice.toLong, None))
      case lineRegex(sku, unitPrice, specialPrice) =>
        parseSpecialPrice(specialPrice).map { specialPrice =>
          Pricing(sku, unitPrice.toLong, Some(specialPrice))
        }
      case _ => Failure(new IllegalArgumentException(s"Could not match $s"))
    }
  }
  def parseSpecialPrice(s: String): Try[SpecialPrice] = Try {
    val specialPriceRegex(quantity, price) = s
    SpecialPrice(quantity.toInt, price.toLong)
  }

}
