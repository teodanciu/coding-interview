package teozaurus.checkout

import scala.concurrent.{Await, Future, blocking}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import com.typesafe.scalalogging.LazyLogging
import scala.io.Source

object MainApp extends App with LazyLogging {

  val fileName = "checkout"

  def run(): Future[Unit] =
    Future { blocking {
      val lines = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(fileName)).getLines()
      val (rules, invalids) = Parser.parse(lines)
      if (invalids.nonEmpty)
        logger.warn(s"Failed to parse the following lines: ${invalids.mkString("\n")}")
      logger.info(s"Loaded pricing rules: ${rules.rules.mkString("\n")}")
      val price = PriceCalculator.calculate(
        Checkout(Seq(
          Item("B"),
          Item("A"),
          Item("B")
        )),
        rules
      )
      println(s"Price of B, A, B is $price")
    }}

  Await.result(run(), 1.minute)

}
