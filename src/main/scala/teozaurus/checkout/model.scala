package teozaurus
package checkout

case class Item(sku: String)

case class Pricing(
  sku: String,
  unitPrice: Long,
  specialPrice: Option[SpecialPrice]
)
case class SpecialPrice(
  quantity: Int,
  price: Long
)
case class PricingRules(rules: Seq[Pricing])
case class Checkout(items: Seq[Item])

class PriceNotFoundException(sku: String) extends Exception