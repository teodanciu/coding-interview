package teozaurus.checkout

import scala.util.Try

object PriceCalculator {

  def calculate(checkout: Checkout, pricingRules: PricingRules): Try[Long]  = Try {
    val grouped: Seq[(String, Seq[Item])] = checkout.items.groupBy(_.sku).toSeq
    grouped.foldLeft(0L) { case (acc, (sku, items)) =>
      val pricing = pricingRules.rules.find(_.sku == sku).getOrElse(throw new PriceNotFoundException(sku))
      val price = pricing.specialPrice.fold(pricing.unitPrice * items.size) { specialPrice =>
        val special = items.size / specialPrice.quantity
        val unit = items.size % specialPrice.quantity
        special * specialPrice.price + unit * pricing.unitPrice
      }
      acc + price
    }
  }
}
