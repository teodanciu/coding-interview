package teozaurus.addressbook

import org.joda.time.LocalDate
import scala.util.{Failure, Success, Try}

sealed trait Gender
object Gender {
  case object Male extends Gender
  case object Female extends Gender

  val values = Set(Male, Female)

  def fromString(s: String): Try[Gender] =
    values.find(v => v.toString.toLowerCase == s.toLowerCase) match {
      case Some(v) => Success(v)
      case None => Failure(
        new IllegalArgumentException(s"Unexpected value for gender $s. Expected one of: $values")
      )
    }
}

case class Contact(
  names: Seq[String],
  gender: Gender,
  dateOfBirth: LocalDate
) {
  def name = names.mkString(" ")
}

object Contact {
  def apply(name: String, gender: Gender, dateOfBirth: LocalDate) =
    new Contact(split(name), gender, dateOfBirth)

  lazy val split: String => Seq[String] = _.split("\\s+").toSeq
}

case class AddressBook(
  contacts: Seq[Contact]
) {
  val isEmpty = contacts.isEmpty
  def lookup(names: Seq[String]): Option[Contact] =
    contacts.find(_.names.toSet.intersect(names.toSet).nonEmpty)
  def lookup(name: String): Option[Contact] =
    lookup(Contact.split(name))
}

