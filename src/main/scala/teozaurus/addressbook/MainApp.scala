package teozaurus.addressbook

import teozaurus.addressbook.parser.Parser
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.blocking
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.io.Source

object MainApp extends App with LazyLogging {
  val fileName = "addressbook"

  def answerQueries(addressBook: AddressBook): Unit = {
    println("1. How many males are in the address book?")
    println(s"A: ${Queries.numberOfMales(addressBook)}")
    println("2. Who is the oldest person in the address book?")
    println(s"A: ${Queries.oldest(addressBook).getOrElse("N/A, since address book is empty")}")
    println("3. How many days older is Bill than Paul?")
    println(s"A: ${Queries.ageDifferenceInDays("Bill", "Paul")(addressBook)
      .getOrElse("N/A, since these names were not found in the address book")}"
    )
  }

  def run(fileName: String): Future[Unit] = {
    Future {
      blocking {
        Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(fileName)).getLines
      }
    }.map { lines =>
      val result = Parser.parse(lines)
      if (result.parseFailures.nonEmpty) {
        logger.warn(s"Parsed input with errors: " + result.parseFailures.mkString("\n"))
      }
      val addressBook = result.addressBook
      logger.info(s"Parsed addressbook with ${addressBook.contacts.size} contacts")
      logger.debug(s"Parsed addressbook: $addressBook")
      answerQueries(addressBook)
    }
  }
  Await.result(run(fileName), 1.minute)
}
