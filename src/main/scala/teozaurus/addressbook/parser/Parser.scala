package teozaurus
package addressbook
package parser

import date.Dates
import org.joda.time.LocalDate
import scala.util.{Failure, Success, Try}

object Parser {

  val columnCount = 3

  def parse(lines: Iterator[String]): ParseResult = {
    val (contacts, invalids) =
      lines.foldRight((Seq.empty[Contact], Seq.empty[InvalidInput])) { case (line, (contacts, invalids)) =>
        parseLine(line) match {
          case Left(i) => (contacts, i +: invalids)
          case Right(c) => (c +: contacts, invalids)
        }
    }
    ParseResult(AddressBook(contacts), invalids)
  }

  private def parseLine(line: String): Either[InvalidInput, Contact] = {
    val split = splitLine(line)
    if (split.size != columnCount)
      Left(InvalidInput(line, new InvalidFormatException))
    else {
      val tryContact = split match {
        case Seq(names, gender, dob) =>
          parseGender(gender).flatMap { gender =>
            parseDate(dob).map { dob =>
              Contact(parseNames(names), gender, dob)
            }
          }
      }
      tryContact match {
        case Success(c) => Right(c)
        case Failure(e) => Left(InvalidInput(line, e))
      }
    }
  }

  def splitLine(line: String): Seq[String] =
    line.split("\\s*,\\s*").toSeq

  def parseGender(g: String): Try[Gender] =
    Gender.fromString(g).recoverWith {
      case e => Failure(new InvalidGenderException(e))
    }

  def parseDate(d: String): Try[LocalDate] =
    Try(LocalDate.parse(d, Dates.format)).recoverWith {
      case e => Failure(new InvalidDateException(e))
    }

  def parseNames(names: String): Seq[String] = {
    val split = Contact.split(names)
    if (split.isEmpty) Seq(names) else split
  }

}
