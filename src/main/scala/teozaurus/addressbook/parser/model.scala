package teozaurus.addressbook
package parser

case class InvalidInput(input: String, t: Throwable)

case class ParseResult(
  addressBook: AddressBook,
  parseFailures: Seq[InvalidInput]
)

class InvalidGenderException(cause: Throwable) extends Exception(cause)
class InvalidFormatException extends Exception
class InvalidDateException(cause: Throwable) extends Exception