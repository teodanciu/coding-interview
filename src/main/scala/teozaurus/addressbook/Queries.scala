package teozaurus
package addressbook

import date.Dates
import Dates._
import org.joda.time.Days

object Queries {

  def numberOfMales(addressBook: AddressBook): Int =
    addressBook.contacts.count(_.gender == Gender.Male)

  def oldest(addressBook: AddressBook): Option[Contact] =
    addressBook.contacts.headOption.map(_ => addressBook.contacts.minBy(_.dateOfBirth))

  def ageDifferenceInDays(name1: String, name2: String)(addressBook: AddressBook): Option[Int] = {
    addressBook.lookup(name1).flatMap { c1 =>
      addressBook.lookup(name2).map { c2 =>
        Days.daysBetween(c1.dateOfBirth, c2.dateOfBirth).getDays
      }
    }
  }
}
